package tests;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static org.testng.Assert.*;

public class TestPokes {

    private Properties env = new Properties();

    private static final String POKEMON_PIKACHU_RESOURCE="/pokemon/pikachu";

    private RequestSpecification httpRequest;
    // Response response = httpRequest.request(Method.GET, "/pokemon/pikachu");

    @BeforeClass
    public void setEnvironment() throws IOException {
        InputStream fileInputStream = getClass().getClassLoader().getResourceAsStream("env.properties");
        env.load(fileInputStream);

        RestAssured.baseURI = env.getProperty("HOST");
        httpRequest = RestAssured.given();
    }

    @Test
    public void testPikachuResponseStatus(){

        Response response = httpRequest.get(POKEMON_PIKACHU_RESOURCE);

        String statusLine = response.getStatusLine();
        int statusCode = response.getStatusCode();

        assertEquals(statusLine, "HTTP/1.1 200 OK", "Verify correct status line");
        assertEquals(statusCode, 200, "Verify correct status code");

        //no relacionado
        //httpRequest.param("location","-33").when().get(POKEMON_PIKACHU_RESOURCE).then().assertThat().statusCode(200);.
    }

    @Test
    public void postDataPikachu() {

    }

    @Test
    public void testPikachuResponseHeader(){

        Response response = httpRequest.get(POKEMON_PIKACHU_RESOURCE);

        String expectedContentLength = "4763";
        String actualContentLength = response.header("Content-Length");

        assertEquals(expectedContentLength, actualContentLength, "Verify Content Length is "+expectedContentLength);

    }

    @Test
    public void testPikachuResponseAllHeaders(){

        Response response = httpRequest.get(POKEMON_PIKACHU_RESOURCE);

        Headers allHeaders = response.headers();

        for (Header header: allHeaders) {
            System.out.println("Key: "+ header.getName() + " Value: "+ header.getValue());
        }

    }

    @Test
    public void testPikachuResponseBody(){

        Response response = httpRequest.log().all().get(POKEMON_PIKACHU_RESOURCE);

        ResponseBody body = response.getBody();

        String bodyString = body.asString();

        assertTrue(bodyString.contains("lightning-rod"), "Body doesn't contain lightning-rod");
    }

    @Test
    public void testPikachuResponseBodyContainsStatic() {

        Response response = httpRequest.get(POKEMON_PIKACHU_RESOURCE);

        JsonPath jsonPathEvaluator = response.jsonPath();

        int baseExp = jsonPathEvaluator.get("base_experience");
        assertEquals(baseExp, 112, "Base Experience don't match");

        //System.out.println( jsonPathEvaluator.get("abilities[0].ability"));

        String ability = jsonPathEvaluator.get("abilities.ability[0].name");
        assertEquals(ability, "lightning-rod", "Different Abilities");

    }

}
