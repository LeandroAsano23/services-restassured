package classes;

import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Utilities {

    public static XmlPath rawToXmlPath(Response response){
        return new XmlPath(response.toString());
    }

    public static JsonPath rawToJsonPath(Response response){
        return new JsonPath(response.toString());
    }

    public static String generateStringFromResource(String file) throws IOException {
        return new String(Files.readAllBytes(Paths.get(file)));
    }

}
